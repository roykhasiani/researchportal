<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1" name="viewport"><!--===============================================================================================-->
	<link href="images/icons/favicon.ico" rel="icon" type="image/png"><!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
	<link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('vendor/animate/animate.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('vendor/animsition/css/animsition.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
	<link href="{{ asset('css/util.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"><!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<img alt="Person" class="w3-margin" src="{{asset('images/sbs.png')}}" style="width:100%">
				<span class="login100-form-title p-b-41">Login</span>
				<form action="" class="login100-form validate-form p-b-33 p-t-5">
					<div class="wrap-input100 validate-input" data-validate="Enter username">
						<input class="input100" name="username" placeholder="User ID" type="text"> <span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" name="pass" placeholder="Password" type="password"> <span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div><!--===============================================================================================-->
	<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('vendor/animsition/js/animsition.min.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('vendor/bootstrap/js/popper.js')}}">
	</script> 
	<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('vendor/select2/select2.min.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('vendor/daterangepicker/moment.min.js')}}">
	</script> 
	<script src="{{asset('vendor/daterangepicker/daterangepicker.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('vendor/countdowntime/countdowntime.js')}}">
	</script> <!--===============================================================================================-->
	 
	<script src="{{asset('js/main.js')}}">
	</script>
</body>
</html>