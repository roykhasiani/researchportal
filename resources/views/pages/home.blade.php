<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet"><noscript>
	<link href="assets/css/noscript.css" rel="stylesheet"></noscript>
	<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
	           * {
	               font-family: Ubuntu;
	           }
	</style>
</head>
<body class="is-preload">
	<!-- Sidebar -->
	<section id="sidebar" style="font-family: Ubuntu;">
		<div class="inner">
			<nav>
				<img alt="Person" class="w3-margin" src="{{asset('images/sbs.png')}}" style="width:100%">
				<ul>
					<li>
						<img alt="Person" class="w3-margin w3-circle" src="{{asset('images/avatar.png')}}" style="width:50%">
						<p style="padding-right:14%">Roy Murwa</p>
					</li>
					<li><a href="#intro">Submit a Thesis</a></li>
					<li><a href="#one">Your Recent Theses</a></li>
					<li><a href="#two">Latest Theses</a></li>
					<li><a href="{{URL::to('/all')}}">All Theses</a></li>
					<li><a href="{{URL::to('/admin')}}">Admin Area</a></li>
					<li><a href="#logout">Log Out</a></li>
				</ul>
			</nav>
		</div>
	</section><!-- Wrapper -->
	<div id="wrapper">
		<!-- Intro -->
		<section class="wrapper style1 fullscreen fade-up" id="intro" style="font-family: Ubuntu; background-image: url('images/bg-01.jpg');">
			<div class="inner">
				<h2 class="w3-center" style="font-family: Ubuntu;">Enter Your Thesis Details Below :</h2><br>
				{{--
				<div class="split style1">
					--}}
					<section>
						<form action="#" method="post">
							<div class="fields">
								<div class="field half">
									<label for="name">Thesis Title :</label> <input id="name" name="doc_title" type="text">
								</div>
								<div class="field half">
									<label for="file">Select Document :</label> <input id="tags" name="doc_file" type="file">
								</div>
								<div class="field">
									<label for="message">Short Description :</label> 
									<textarea id="doc_description" name="doc_description" required="" rows="5"></textarea>
								</div>
							</div>
							<ul class="actions">
								<li><a class="button submit" href="">Submit Here &gt;&gt;</a></li>
							</ul>{{--
						</form>
					</section>
				</div>--}}
				<hr>
				<section class="fade-up" id="one">
					<div class="inner">
						<h2 class="w3-center" style="font-family: Ubuntu;">Some of Your Recent Documents :</h2><br>
						<div class="features">
							<table class="w3-round-xlarge" id="id01" style="background:transparent">
								<tr>
									<th>Document ID</th>
									<th>Title</th>
									<th>Date Submitted</th>
								</tr>
								<tr class="item">
									<td>Alfreds Futterkiste</td>
									<td>Berlin</td>
									<td>Germany</td>
								</tr>
								<tr class="item">
									<td>Berglunds snabbköp</td>
									<td>Lule</td>
									<td>Sweden</td>
								</tr>
								<tr class="item">
									<td>Centro comercial Moctezuma</td>
									<td>México D.F.</td>
									<td>Mexico</td>
								</tr>
								<tr class="item">
									<td>Ernst Handel</td>
									<td>Graz</td>
									<td>Austria</td>
								</tr>
							</table>
						</div>
					</div>
					<ul class="actions">
						<li><a class="button" href="{{URL::to('/all')}}">Browse All Your Theses &gt;&gt;</a></li>
					</ul>
					<hr>
					<section class="fade-up" id="two" style="font-family: Ubuntu">
						<div class="inner">
							<h2 class="w3-center" style="font-family: Ubuntu;">Latest Theses Uploaded :</h2><br>
							<div class="features">
								<table class="w3-round-xlarge" id="id01" style="background:transparent">
									<tr>
										<th>Author ID</th>
										<th>Title</th>
										<th>Date Submitted</th>
									</tr>
									<tr class="item">
										<td>Alfreds Futterkiste</td>
										<td>Berlin</td>
										<td>Germany</td>
									</tr>
									<tr class="item">
										<td>Berglunds snabbköp</td>
										<td>Lule</td>
										<td>Sweden</td>
									</tr>
									<tr class="item">
										<td>Centro comercial Moctezuma</td>
										<td>México D.F.</td>
										<td>Mexico</td>
									</tr>
									<tr class="item">
										<td>Ernst Handel</td>
										<td>Graz</td>
										<td>Austria</td>
									</tr>
									<tr class="item">
										<td>FISSA Fabrica Inter. Salchichas S.A.</td>
										<td>Madrid</td>
										<td>Spain</td>
									</tr>
									<tr class="item">
										<td>Galería del gastrónomo</td>
										<td>Barcelona</td>
										<td>Spain</td>
									</tr>
									<tr class="item">
										<td>Island Trading</td>
										<td>Cowes</td>
										<td>UK</td>
									</tr>
									<tr class="item">
										<td>Königlich Essen</td>
										<td>Brandenburg</td>
										<td>Germany</td>
									</tr>
									<tr class="item">
										<td>Laughing Bacchus Wine Cellars</td>
										<td>Vancouver</td>
										<td>Canada</td>
									</tr>
								</table>
							</div>
							<ul class="actions">
								<li><a class="button" href="{{URL::to('/all')}}">Go To All Theses &gt;&gt;</a></li>
							</ul>
						</div>
					</section>
				</section>
			</div>
		</section>
	</div><!-- Footer -->
	<footer class="wrapper style1-alt" id="footer">
		<div class="inner">
			<ul class="menu w3-center">
				<li>&copy; SBS. All rights reserved.</li>
			</ul>
		</div>
	</footer><!-- Scripts -->
	<script src="assets/js/jquery.min.js">
	</script> 
	<script src="assets/js/jquery.scrollex.min.js">
	</script> 
	<script src="assets/js/jquery.scrolly.min.js">
	</script> 
	<script src="assets/js/browser.min.js">
	</script> 
	<script src="assets/js/breakpoints.min.js">
	</script> 
	<script src="assets/js/util.js">
	</script> 
	<script src="assets/js/main.js">
	</script>
</body>
</html>