<!DOCTYPE html>
<html>
<head>
	<title>Admin Dashboard</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet"><noscript>
	<link href="assets/css/noscript.css" rel="stylesheet"></noscript>
	<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
	           * {
	               font-family: Ubuntu;
	           }
	</style>
</head>
<body class="is-preload">
	<!-- Sidebar -->
	<section id="sidebar" style="font-family: Ubuntu;">
		<div class="inner">
			<nav>
				<img alt="Person" class="w3-margin" src="{{asset('images/sbs.png')}}" style="width:100%">
				<ul>
					<li>
						<img alt="Person" class="w3-margin w3-circle" src="{{asset('images/avatar.png')}}" style="width:50%">
						<p style="padding-right:14%">Admin</p>
					</li>
					<li><a href="#intro">Add a Course</a></li>
					<li><a href="#one">Add a Course Category</a></li>
					<li><a href="{{URL::to('/theses')}}">Uploaded Theses</a></li>
					<li><a href="#logout">Log Out</a></li>
				</ul>
			</nav>
		</div>
	</section><!-- Wrapper -->
	<div id="wrapper">
		<!-- Intro -->
		<section class="wrapper style1 fullscreen fade-up" id="intro" style="font-family: Ubuntu; background-image: url('images/bg-01.jpg');">
			<div class="inner">
				<h2 class="w3-center" style="font-family: Ubuntu;">Add a New Course Below :</h2><br>
				{{--
				<div class="split style1">
					--}}
					<section>
						<form action="#" method="post">
							<div class="fields">
								<div class="field third">
									<label for="name">Course Full Name :</label> <input id="name" name="course_name" type="text">
                                </div>
                                <div class="field third">
									<label for="course_short_name">Course Short Name :</label> <input id="name" name="course_short_name" type="text">
                                </div>
                                <div class="field third">
									<label for="course_category">Course Category :</label> <input id="name" name="course_category" type="text">
								</div>
								<div class="field">
									<label for="message">Course Description :</label> 
									<textarea id="course_description" name="course_description" required="" rows="5"></textarea>
								</div>
							</div>
							<ul class="actions">
								<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<li><a class="button submit" href="">Submit Here &gt;&gt;</a></li> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
							</ul>
						</form>
					{{-- </section>
				</div>--}}
				<hr>
				<section class="fade-up" id="one">
					<div class="inner">
						<h2 class="w3-center" style="font-family: Ubuntu;">Add a New Course Category :</h2><br>
				{{--
				<div class="split style1">
					--}}
					<section>
						<form action="#" method="post">
							<div class="fields">
								
                                <div class="field full">
									<label for="course_category">Course Category :</label> <input id="name" name="course_category" type="text">
								</div>
								<div class="field">
									<label for="message">Category Description :</label> 
									<textarea id="category_description" name="category_description" required="" rows="5"></textarea>
								</div>
							</div>
							<ul class="actions">
								<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<li><a class="button submit" href="">Submit Here &gt;&gt;</a></li> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
							</ul>
						</form>
                    <hr>
			</div>
		</section>
	</div><!-- Footer -->
	<footer class="wrapper style1-alt" id="footer">
		<div class="inner">
			<ul class="menu w3-center">
				<li>&copy; SBS. All rights reserved.</li>
			</ul>
		</div>
	</footer><!-- Scripts -->
	<script src="assets/js/jquery.min.js">
	</script> 
	<script src="assets/js/jquery.scrollex.min.js">
	</script> 
	<script src="assets/js/jquery.scrolly.min.js">
	</script> 
	<script src="assets/js/browser.min.js">
	</script> 
	<script src="assets/js/breakpoints.min.js">
	</script> 
	<script src="assets/js/util.js">
	</script> 
	<script src="assets/js/main.js">
	</script>
</body>
</html>