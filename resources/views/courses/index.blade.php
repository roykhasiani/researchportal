<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet"><noscript>
	<link href="assets/css/noscript.css" rel="stylesheet"></noscript>
	<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
	           * {
	               font-family: Ubuntu;
	           }
	</style>
</head>
<body class="is-preload">
	<!-- Sidebar -->
	<section id="sidebar" style="font-family: Ubuntu;">
		<div class="inner">
			<nav>
				<img alt="Person" class="w3-margin" src="{{asset('images/sbs.png')}}" style="width:100%">
				<ul>
					<li>
						<img alt="Person" class="w3-margin w3-circle" src="{{asset('images/avatar.png')}}" style="width:50%">
						<p style="padding-right:14%">Admin</p>
					</li>
					<li style="list-style: none">{{--</li>
					<li><a href="#intro">Submit Thesis</a></li>
					<li style="list-style: none">--}}</li>
					<li><a href="{{URL::to('/home')}}">Back Home</a></li>
					<li style="list-style: none">{{--</li>
					<li><a href="#two">Latest Theses</a></li>
					<li style="list-style: none">--}}</li>
					<li><a href="{{asset('my')}}">My Theses</a></li>
					<li><a href="{{URL::to('/admin')}}">Admin Area</a></li>
					<li><a href="#logout">Log Out</a></li>
				</ul>
			</nav>
		</div>
	</section><!-- Wrapper -->
	<div id="wrapper">
		<!-- Intro -->
		<section class="wrapper style1 fullscreen fade-up" id="intro" style="font-family: Ubuntu; background-image: url('images/bg-01.jpg');">
			<div class="inner">
				<h3 class="w3-center" style="font-family: Ubuntu;">Search for a Course :</h3><br>
				<p><input class="w3-input w3-round-xlarge w3-border" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search.." style="background: transparent;"></p>{{--
				<div class="split style1">
					--}}
					<section>
						<section class="fade-up" id="one">
							<div class="inner">
								<h3 class="w3-center" style="font-family: Ubuntu;">Results :</h3>
								<div class="features">
                                    @if(count($courses)>0)

                                        <table class="w3-round-xlarge" id="id01" style="background:transparent">
                                            <tr>
                                                <th>Course ID</th>
                                                <th>Course Title</th>
                                                <th>Course Short Name</th>
                                            </tr>

                                                @foreach($courses as $course)

                                                    <tr class="item">
                                                        <td>{{$course->id}}</td>
                                                        <td>{{$course->course_name}}</td>
                                                        <td>{{$course->short_name}}</td>
                                                    </tr>
                                                    
                                                @endforeach    

                                        </table>

                                    @else
                                        <h3>No Courses Found</h3>
                                    @endif
								</div>
							</div>
						</section>
					</section>
				</div>
			</div>
		</section><!-- Footer -->
		<footer class="wrapper style1-alt" id="footer">
			<div class="inner">
				<ul class="menu w3-center">
					<li>&copy; SBS. All rights reserved.</li>
				</ul>
			</div>
		</footer><!-- Scripts -->
		<script src="assets/js/jquery.min.js">
		</script> 
		<script src="assets/js/jquery.scrollex.min.js">
		</script> 
		<script src="assets/js/jquery.scrolly.min.js">
		</script> 
		<script src="assets/js/browser.min.js">
		</script> 
		<script src="assets/js/breakpoints.min.js">
		</script> 
		<script src="assets/js/util.js">
		</script> 
		<script src="assets/js/main.js">
		</script> 
	</div>
</body>
</html>