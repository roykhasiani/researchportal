<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet"><noscript>
	<link href="assets/css/noscript.css" rel="stylesheet"></noscript>
	<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
	           * {
	               font-family: Ubuntu;
	           }
	</style>
</head>
<body class="is-preload">
	<!-- Sidebar -->
	<section id="sidebar" style="font-family: Ubuntu;">
		<div class="inner">
			<nav>
				<img alt="Person" class="w3-margin" src="{{asset('images/sbs.png')}}" style="width:100%">
				<ul>
					<li>
						<img alt="Person" class="w3-margin w3-circle" src="{{asset('images/avatar.png')}}" style="width:50%">
						<p style="padding-right:14%">Roy Murwa</p>
					</li>
					<li style="list-style: none">{{--</li>
					<li><a href="#intro">Submit Thesis</a></li>
					<li style="list-style: none">--}}</li>
					<li><a href="{{URL::to('/home')}}">Back Home</a></li>
					<li style="list-style: none">{{--</li>
					<li><a href="#two">Latest Theses</a></li>
					<li style="list-style: none">--}}</li>
					<li><a href="{{asset('my')}}">My Theses</a></li>
					<li><a href="{{URL::to('/admin')}}">Admin Area</a></li>
					<li><a href="#logout">Log Out</a></li>
				</ul>
			</nav>
		</div>
	</section><!-- Wrapper -->
	<div id="wrapper">
		<!-- Intro -->
		<section class="wrapper style1 fullscreen fade-up" id="intro" style="font-family: Ubuntu; background-image: url('images/bg-01.jpg');">
			<div class="inner">
				<h3 class="w3-center" style="font-family: Ubuntu;">Search for a Thesis :</h3><br>
				<p><input class="w3-input w3-round-xlarge w3-border" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search.." style="background: transparent;"></p>{{--
				<div class="split style1">
					--}}
					<section>
						<section class="fade-up" id="one">
							<div class="inner">
								<h3 class="w3-center" style="font-family: Ubuntu;">Results :</h3>
								<div class="features">
									<table class="w3-round-xlarge" id="id01" style="background:transparent">
										<tr>
											<th>User ID</th>
											<th>Title</th>
											<th>Date Submitted</th>
										</tr>
										<tr class="item">
											<td>Alfreds Futterkiste</td>
											<td>Berlin</td>
											<td>Germany</td>
										</tr>
										<tr class="item">
											<td>Berglunds snabbköp</td>
											<td>Lule</td>
											<td>Sweden</td>
										</tr>
										<tr class="item">
											<td>Centro comercial Moctezuma</td>
											<td>México D.F.</td>
											<td>Mexico</td>
										</tr>
										<tr class="item">
											<td>Ernst Handel</td>
											<td>Graz</td>
											<td>Austria</td>
										</tr>
										<tr class="item">
											<td>FISSA Fabrica Inter. Salchichas S.A.</td>
											<td>Madrid</td>
											<td>Spain</td>
										</tr>
										<tr class="item">
											<td>Galería del gastrónomo</td>
											<td>Barcelona</td>
											<td>Spain</td>
										</tr>
										<tr class="item">
											<td>Island Trading</td>
											<td>Cowes</td>
											<td>UK</td>
										</tr>
										<tr class="item">
											<td>Königlich Essen</td>
											<td>Brandenburg</td>
											<td>Germany</td>
										</tr>
										<tr class="item">
											<td>Laughing Bacchus Wine Cellars</td>
											<td>Vancouver</td>
											<td>Canada</td>
										</tr>
										<tr class="item">
											<td>Magazzini Alimentari Riuniti</td>
											<td>Bergamo</td>
											<td>Italy</td>
										</tr>
										<tr class="item">
											<td>North/South</td>
											<td>London</td>
											<td>UK</td>
										</tr>
										<tr class="item">
											<td>Paris spécialités</td>
											<td>Paris</td>
											<td>France</td>
										</tr>
										<tr class="item">
											<td>Rattlesnake Canyon Grocery</td>
											<td>Albuquerque</td>
											<td>USA</td>
										</tr>
										<tr class="item">
											<td>Simons bistro</td>
											<td>København</td>
											<td>Denmark</td>
										</tr>
										<tr class="item">
											<td>The Big Cheese</td>
											<td>Portland</td>
											<td>USA</td>
										</tr>
										<tr class="item">
											<td>Vaffeljernet</td>
											<td>Århus</td>
											<td>Denmark</td>
										</tr>
										<tr class="item">
											<td>Wolski Zajazd</td>
											<td>Warszawa</td>
											<td>Poland</td>
										</tr>
									</table>
								</div>
							</div>
						</section>
					</section>
				</div>
			</div>
		</section><!-- Footer -->
		<footer class="wrapper style1-alt" id="footer">
			<div class="inner">
				<ul class="menu w3-center">
					<li>&copy; SBS. All rights reserved.</li>
				</ul>
			</div>
		</footer><!-- Scripts -->
		<script src="assets/js/jquery.min.js">
		</script> 
		<script src="assets/js/jquery.scrollex.min.js">
		</script> 
		<script src="assets/js/jquery.scrolly.min.js">
		</script> 
		<script src="assets/js/browser.min.js">
		</script> 
		<script src="assets/js/breakpoints.min.js">
		</script> 
		<script src="assets/js/util.js">
		</script> 
		<script src="assets/js/main.js">
		</script> 
	</div>
</body>
</html>