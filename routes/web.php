<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@login');
Route::get('/home', 'PagesController@index');
Route::get('/all', 'PagesController@all');
Route::get('/my', 'PagesController@my');
Route::get('/admin', 'PagesController@admin');
Route::resource('theses', 'ThesesController');
Route::resource('courses', 'CoursesController');
Route::resource('categories', 'CategoriesContoller');
Route::resource('tags', 'TagsController');